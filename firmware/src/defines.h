#ifndef DEFINES_H
#define DEFINES_H 1

#define BATTERY_PIN A6
#define MOTOR_STEP_PIN 6
#define MOTOR_DIRECTION_PIN 4
#define MOTOR_MS1_PIN A1
#define MOTOR_MS2_PIN A2
#define MOTOR_MS3_PIN A3
#define MOTOR_ENABLE_PIN A0
#define MOTOR_ENDSTOP 2

#define KEY_PIN 9
#define BT_TX 11
#define BT_RX 10

#define pinLED 13

#define DEBUG true

#endif
