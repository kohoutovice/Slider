#ifndef BLUETOOTH_H
#define BLUETOOTH_H 1

#include <Arduino.h>
#include <SoftwareSerial.h>
#include "defines.h"


class Bluetooth : public SoftwareSerial
{
    using SoftwareSerial::SoftwareSerial;

private:
    uint8_t key_pin;

public:
    void begin(uint8_t key);

    void send_string(String str){
#ifdef DEBUG
      Serial.print("Sending");
      Serial.println(str);
#endif
      println(str);
    }

    void send_command(int command){
      String r = String("C") + String(command,DEC) + String(";") + String('\n');
      send_string(r);
    };
    void send_command(int command, double value){
      String r = String("C") + String(command,DEC)+ String("V") + String(value,2) + String(";") + String('\n');
      send_string(r);
    }
    void send_command(int command, int value){
      String r = String("C") + String(command,DEC)+ String("V") + String(value,DEC) + String(";") + String('\n');
      send_string(r);
    }
    void send_command(int command, long value){
      String r = String("C") + String(command,DEC)+ String("V") + String(value,DEC) + String(";") + String('\n');
      send_string(r);
    }
    void send_command(int command, String error){
      String r = String("C") + String(command,DEC)+ String("E") + error + String(";") + String('\n');
      send_string(r);
    }


};


#endif
