#include "bluetooth.h"


void Bluetooth::begin(uint8_t key){
    SoftwareSerial::begin(9600);
    key_pin = key;
    pinMode(key_pin, OUTPUT);    // this pin will pull the HC-05 pin 34 (KEY pin) HIGH to switch                            module to AT mode
    digitalWrite(key_pin, HIGH);

    delay(1000);
    write("AT+NAME=Slider");                          //  SET Module to " Program New Name "
    write("\r\n");
    delay(1000);
    println("AT+BAUD4");                           //  SET Baud SPEED  "to Comm with Module "
    delay(1000);
    write("AT+PIN");                                //  SET MOdule to " new Password "
    write("1234");                                   //  SET " Password "      default = ( 1234 )
    write("\r\n");
    delay(1000);
    digitalWrite(key_pin, LOW);
    SoftwareSerial::begin(9600);
}
