package com.jjurca.jslide.ui.machine;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.jjurca.jslide.Machines;
import com.jjurca.jslide.R;

import it.sephiroth.android.library.numberpicker.NumberPicker;

public class MachineFragment extends Fragment {

    private MachineViewModel autoViewModel;
    private NumberPicker hours_widget;
    private NumberPicker seconds_widget;
    private NumberPicker minutes_widget;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        autoViewModel = ViewModelProviders.of(this).get(MachineViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_auto, container, false);

        hours_widget = root.findViewById(R.id.hourValue);
        minutes_widget = root.findViewById(R.id.minuteValue);
        seconds_widget = root.findViewById(R.id.secondValue);

        Button button = (Button) root.findViewById(R.id.btn_start);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int hours = hours_widget.getProgress();;
                int minutes = minutes_widget.getProgress();
                int seconds = seconds_widget.getProgress();
                int total_secs = seconds + minutes*60 + hours*60*60;

            }
        });



        return root;
    }


}