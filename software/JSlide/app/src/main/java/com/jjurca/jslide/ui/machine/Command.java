package com.jjurca.jslide.ui.machine;

public enum Command{
    ping(0),
    setDirection(1),
    setTime(2),
    setRpmspeed(3),
    start(4),
    stop(5),
    home(6),
    getPosition(7),
    getLength(8),
    enable(9), //<1=true;0=false>
    pause(10),
    getStepSize(11),
    getVoltage(12),
    getState(13),
    getDirection(14),
    getStepDelay(15),
    getMode(16),
    isHomed(17),
    isEnabled(18),
    isSensorPushed(19),
    getTotalTimeSpeed(20),
    moveToPosition(21)
    ;

    private Integer numVal;
    Command(int numVal) {  this.numVal = numVal; }
    public Integer getNumVal() { return numVal; }
}
