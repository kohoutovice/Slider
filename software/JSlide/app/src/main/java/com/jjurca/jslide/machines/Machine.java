package com.jjurca.jslide.machines;

import android.util.Log;

import com.jjurca.jslide.ui.machine.ConnectionListener;


public class Machine extends BluetoothGenericSerialDevice {
    private final String bluetoothName = "JSlide";


    public Machine(){
        super();
        setConnectionListener(new ConnectionListener(){
            @Override
            public void onReceived(String message) {
                parseInput(message);
            }
        });
    }

    public void parseInput(String msg){
       Log.v("BR_Received", msg );
    }
}




