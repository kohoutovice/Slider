package com.jjurca.jslide.ui.machine;

import android.util.Log;

interface ConnectionListenerInterface {
    void onConnected();
    void onDisconnected();
    void onError();
    void onConnecting();
    void onAddressChanged();
    void onSent(String message);
    void onReceived(String message);
}

public class ConnectionListener implements ConnectionListenerInterface{

    @Override
    public void onConnected() {
        Log.e("Connection", "Not implemented onConnected");
    }

    @Override
    public void onError() {
        Log.e("Connection", "Not implemented onError");
    }

    @Override
    public void onDisconnected() {
        Log.e("Connection", "Not implemented onDisconnected");
    }

    @Override
    public void onConnecting() { Log.e("Connection", "Not implemented onConnecting"); }

    @Override
    public void onAddressChanged() {
        Log.e("Connection", "Not implemented onAddressChanged");
    }

    @Override
    public void onSent(String message) {
        Log.e("Connection", "Not implemented onSent");
    }

    @Override
    public void onReceived(String message) {
        Log.e("Connection", "Not implemented onReceived");
    }


}
