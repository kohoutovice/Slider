package com.jjurca.jslide.ui.settings;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.jjurca.jslide.Machines;
import com.jjurca.jslide.R;

public class SettingsFragment extends Fragment {

    private SettingsViewModel settingsViewModel;

    private TextView batteryVoltage;
    private TextView deviceState;
    private TextView deviceName;
    private TextView log;
    private Button refreshBtn;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        settingsViewModel = ViewModelProviders.of(this).get(SettingsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        batteryVoltage = root.findViewById(R.id.baterryVoltage);
        deviceState = root.findViewById(R.id.deviceState);
        deviceName = root.findViewById(R.id.deviceName);
        log = root.findViewById(R.id.log);
        refreshBtn = root.findViewById(R.id.refreshBtn);


        setObservers();

        return root;
    }


    public void setObservers(){
       /*
        Machines.slider.deviceLog.observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                log.setText(s);
            }
        });
*/
    }
}