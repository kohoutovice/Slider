#ifndef DEVICE_H
#define DEVICE_H value

#include "globals.h"


class Device {
private:
  const char* m_name = "Slider";
  const long m_max_step = 17200; //TODO
  const char* type = "linear"; // rotary
  const int version = 1;

public:
  Device (){};

  void begin(){
    BTSerial.begin(m_name);

  }

  const char* name(){
    return m_name;
  }

  long maxStep(){
    return m_max_step;
  }



};
#endif
