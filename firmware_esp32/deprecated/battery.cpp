#include "battery.h"

int map_f ( float value, float from1, float to1, float from2, float to2) {
    return (int)((value - from1) / (to1 - from1) * (to2 - from2) + from2);
}


float Battery::getVoltage(){
    float volt_measured = ref_volt*analogRead(pin)/1024.0;
    return volt_measured/resistor_koef;
}

short Battery::getPercentage(){
    int p = map_f(getVoltage(), 6.4, 8.4, 0, 100);
    if (p > 100) {
        p = 100;
    } else if (p < 0) {
        p = 0;
    }
    return p;
}

int Battery::getRemainingTime(){

}

void Battery::update(){

}
