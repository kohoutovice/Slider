#ifndef COMMAND_H
#define COMMAND_H value

#include "globals.h"
#include <string>

void send_string(std::string str){
  BTSerial.println(str.c_str());
}

class Command {
  int m_id;
  const char *description;
  bool isParametrized;
  bool (*f_exec)(int);
public:
  Command(int id, const char* description, bool isParametrized, bool (*f_exec)(int)) :
    m_id(id), description(description), isParametrized(isParametrized), f_exec(f_exec) {};

  int id(){
    return m_id;
  }

  const char* getDescription(){
    return description;
  }

  bool exec(){
    if(isParametrized){
      return false;
    } else {
      return f_exec(0);
    }
  }

  bool exec(int p){
    if (isParametrized) {
      return f_exec(p);
    }
    return false;
  }


  void send_command(){
    std::string r = std::string("C") + std::string(m_id,DEC) + std::string(";") + std::string("\n");
    send_string(r);
  };
  void send_command(double value){
    std::string r = std::string("C") + std::string(m_id,DEC)+ std::string("V") + std::string(value,2) + std::string(";") + std::string("\n");
    send_string(r);
  }
  void send_command(int value){
    std::string r = std::string("C") + std::string(m_id,DEC)+ std::string("V") + std::string(value,DEC) + std::string(";") + std::string("\n");
    send_string(r);
  }
  void send_command(bool value){
    std::string r = std::string("C") + std::string(m_id,DEC)+ std::string("V") + std::string((value)? 1:0,DEC) + std::string(";") + std::string("\n");
    send_string(r);
  }
  void send_command(long value){
    std::string r = std::string("C") + std::string(m_id,DEC)+ std::string("V") + std::string(value,DEC) + std::string(";") + std::string("\n");
    send_string(r);
  }
  void send_command(std::string error){
    std::string r = std::string("C") + std::string(m_id,DEC)+ std::string("E") + error + std::string(";") + std::string("\n");
    send_string(r);
  }

};










#endif
