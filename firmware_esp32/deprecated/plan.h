#ifndef PLAN_H
#define PLAN_H value

class Waypoint {
private:
    /* data */
    int m_duration = 0;
    long m_destination = -1;

public:
    Waypoint (int duration, long destination):
        m_duration(duration), m_destination(destination){};

    bool valid(){
        if (m_duration != -1) {
            return true;
        }
        return false;
    }
    void copy(Waypoint rs){
        m_duration = rs.duration();
        m_destination = rs.destination();
    }

    int duration(){return m_duration;}
    long destination(){return m_destination;}

    void clear(){ m_duration = 0, m_destination = -1;}
};


class Plan {
private:
    Waypoint waypoints[4] = {
        Waypoint(-1, 0),
        Waypoint(-1, 0),
        Waypoint(-1, 0),
        Waypoint(-1, 0)
    };

public:
    Plan (){};

    bool setWaypoint(int index, Waypoint rs){
        waypoints[index].copy(rs);
        return true;
    }

    bool valid(){
        bool ret = true;
        for (int i = 0; i < 4; i++) {
                ret = ret & waypoints[i].valid();
        }
        return ret;
    }

    Waypoint getCurrentWaypoint(long position){
        long i1 = waypoints[0].destination();
        for (int i = 0; i < 4; i++) {
            if (position > i1 && position <= waypoints[i].destination()) {
                return waypoints[i];
            }
            i1 = waypoints[i].destination();
        }
        return Waypoint(-1,-1);
    }

    void clear(){
        for (int i = 0; i < 4; i++) {
            waypoints[i].clear();
        }
    }

};

#endif
