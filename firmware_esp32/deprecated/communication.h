#ifndef COMMUNICATION_H
#define COMMUNICATION_H value


#include <string>
#include <regex>
#include "globals.h"

class Communicator {
private:


public:
    Communicator (){};

    static void communicator_task(Communicator * comm){
        while (true){
            if (BTSerial.available()) {
                comm->process_char(BTSerial.read());
            }
        }
        vTaskDelay(0 / portTICK_PERIOD_MS);
    }

    void process_char(int c){
        typedef enum { S, Command, ReadingCommand, Value, ReadingValue } States;
        int state = S;
        int actualCommand = 0;
        int actualValue = 0;
        std::string buffer = "";

        if (isprint(c)) {
          switch (state) {
              case S:{
                buffer = "";
                if (c == 'C') { state = Command; }
                break;
              }
              case Command:{
                if (c >='0' && c <= '9') {
                  buffer += c;
                  state = ReadingCommand;
                } else {
                  buffer = "";
                  state = S;
                }
                break;
              }
              case ReadingCommand:{
                if (c >='0' && c <= '9') {
                  buffer += c;
                } else if (c == 'V') {
                  actualCommand = std::stoi( buffer );
                  buffer = "";
                  state = Value;
                } else if (c == ';') {
                  actualCommand = std::stoi( buffer );
                  executeCommand(actualCommand);
                  buffer = "";
                  state = S;
                } else {
                  buffer = "";
                  state = S;
                }
                break;
              }
              case Value:{
                if (c >='0' && c <= '9') {
                  buffer += c;
                  state = ReadingValue;
                } else {
                  buffer = "";
                  state = S;
                }
                break;
              }
              case ReadingValue:{
                if (c >='0' && c <= '9') {
                  buffer += c;
                } else if (c == ';') {
                  actualValue = std::stoi( buffer );
                  executeCommand(actualCommand, actualValue);
                  buffer = "";
                  state = S;
                } else {
                  buffer = "";
                  state = S;
                }
                break;
              }
           }
        }
    }


    void executeCommand(int command){};
    void executeCommand(int command, int value){};


};





#endif
